
package es.franam.microserviciopeliculasactorescliente.controladores;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.franam.microserviciopeliculasactorescliente.beans.Rol;
import es.franam.microserviciopeliculasactorescliente.beans.Usuario;
import es.franam.microserviciopeliculasactorescliente.paginador.PageRender;
import es.franam.microserviciopeliculasactorescliente.servicios.IUsuariosService;

@Controller
@RequestMapping("/usuarios")
public class UsuariosController {

	@Autowired
	IUsuariosService usuariosService;

	@GetMapping(value = { "/", "/home", "" })
	public String home(Model model, @RequestParam(name = "page", defaultValue = "0") int page) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Usuario> listado = usuariosService.buscarTodos(pageable);
		PageRender<Usuario> pageRender = new PageRender<Usuario>("/usuarios/listado", listado);
		model.addAttribute("listadoUsuarios", listado);
		model.addAttribute("page", pageRender);
		return "home";

	}

	@GetMapping("/nuevo")
	public String nuevo(Model model) {
		model.addAttribute("titulo", "Nuevo Usuario");
		Usuario usuario = new Usuario();
		model.addAttribute("usuario", usuario);
		return "usuarios/formUsuarios";
	}

	@GetMapping("/buscar")
	public String buscar(Model model) {
		return "usuarios/searchUsuarios";
	}

	@GetMapping("/listado")
	public String listadoUsuarios(Model model, @RequestParam(name = "page", defaultValue = "0") int page) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Usuario> listado = usuariosService.buscarTodos(pageable);
		PageRender<Usuario> pageRender = new PageRender<Usuario>("/usuarios/listado", listado);
		model.addAttribute("titulo", "Listado de todos los Usuarios");
		model.addAttribute("listadoUsuarios", listado);
		model.addAttribute("page", pageRender);
		return "usuarios/listUsuarios";
	}

	@GetMapping("/idUsuario/{id}")
	public String buscarUsuarioPorId(Model model, @PathVariable("id") Integer id) {
		Usuario Usuario = usuariosService.buscarUsuarioPorId(id);
		model.addAttribute("Usuario", Usuario);
		return "Usuarios/formUsuarios";
	}

	@PostMapping("/guardar/")
	public String guardarUsuario(Model model, Usuario usuario, RedirectAttributes attributes) {
		usuariosService.guardar(usuario);
		model.addAttribute("titulo", "Nuevo Usuario");
		attributes.addFlashAttribute("msg", "Los datos del Usuario fueron guardados!");
		return "redirect:/usuarios/listado";
	}

	@GetMapping("/editar/{id}")
	public String editar(Model model, @PathVariable("id") Integer id) {
		Usuario usuario = usuariosService.buscarUsuarioPorId(id);
		model.addAttribute("titulo", "Editar Usuario");
		model.addAttribute("usuario", usuario);

		return "usuarios/formUsuarios";
	}

	@GetMapping("/borrar/{id}")
	public String eliminar(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
		usuariosService.eliminar(id);
		attributes.addFlashAttribute("msg", "Los datos del Usuario fueron borrados!");
		return "redirect:/usuarios/listado";
	}

	@GetMapping("/homeUsuarios")
	public String homeUsuarios() {
		return "usuarios/homeUsuarios";
	}

	@GetMapping("/nombre")
	public String buscarPorNombre(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam("nombre") String nombre) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Usuario> listado = usuariosService.buscarPorNombre(nombre, pageable);
		PageRender<Usuario> pageRender = new PageRender<Usuario>("/listado", listado);
		model.addAttribute("titulo", "Listado de usuarios por nombre");
		model.addAttribute("listadoUsuarios", listado);
		model.addAttribute("page", pageRender);
		return "usuarios/listUsuarios";
	}

	@GetMapping("/correo")
	public String buscarPorCorreo(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam("correo") String correo) {

		Usuario usuario = usuariosService.buscarPorCorreo(correo);
		model.addAttribute("titulo", "Listado de usuarios por correo");
		model.addAttribute("usuario", usuario);

		return "usuarios/listUsuarioCorreo";

	}

	@GetMapping("/signup")
	public String registrarse(Usuario usuario) {
		return "formRegistro";
	}

	@PostMapping("/signup")
	public String guardarRegistro(Usuario usuario, RedirectAttributes attributes) {
		usuario.setEnable(true); // Activado por defecto
		Rol rol = new Rol();
		rol.setIdRol(2); // Perfil USUARIO
		usuario.setRoles(Arrays.asList(rol));
		usuariosService.guardar(usuario);
		attributes.addFlashAttribute("msg", "El registro fue guardado correctamente!");

		return "redirect:/";
	}
}
