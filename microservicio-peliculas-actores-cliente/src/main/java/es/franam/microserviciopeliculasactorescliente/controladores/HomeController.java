package es.franam.microserviciopeliculasactorescliente.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import es.franam.microserviciopeliculasactorescliente.beans.Pelicula;
import es.franam.microserviciopeliculasactorescliente.paginador.PageRender;
import es.franam.microserviciopeliculasactorescliente.servicios.IPeliculasService;

@Controller
public class HomeController {

	@Autowired
	IPeliculasService peliculasService;

	@GetMapping(value = { "/", "/home", "" })
	public String home(Model model, @RequestParam(name = "page", defaultValue = "0") int page) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarTodas(pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/peliculas/listado", listado);
		// model.addAttribute("titulo", "Listado de todos los Peliculas");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "home";

	}
}
