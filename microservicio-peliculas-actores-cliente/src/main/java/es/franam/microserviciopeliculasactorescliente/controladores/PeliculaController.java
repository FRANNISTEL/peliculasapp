package es.franam.microserviciopeliculasactorescliente.controladores;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.franam.microserviciopeliculasactorescliente.beans.Actor;
import es.franam.microserviciopeliculasactorescliente.beans.Critica;
import es.franam.microserviciopeliculasactorescliente.beans.Pelicula;
import es.franam.microserviciopeliculasactorescliente.beans.Rol;
import es.franam.microserviciopeliculasactorescliente.beans.Usuario;
import es.franam.microserviciopeliculasactorescliente.paginador.PageRender;
import es.franam.microserviciopeliculasactorescliente.servicios.IActoresService;
import es.franam.microserviciopeliculasactorescliente.servicios.ICriticasService;
import es.franam.microserviciopeliculasactorescliente.servicios.IPeliculasService;
import es.franam.microserviciopeliculasactorescliente.servicios.IUploadFileService;
import es.franam.microserviciopeliculasactorescliente.servicios.IUsuariosService;

@Controller
@RequestMapping("/peliculas")
public class PeliculaController {

	@Autowired
	IPeliculasService peliculasService;

	@Autowired
	IActoresService actoresService;

	@Autowired
	IUsuariosService usuarioService;

	@Autowired
	private IUploadFileService uploadFileService;
	
	@Autowired
	ICriticasService criticasService;

	@GetMapping("/uploads/{filename:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String filename) {

		Resource recurso = null;

		try {
			recurso = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"")
				.body(recurso);
	}

	@GetMapping("/nuevo")
	public String nuevo(Model model) {
		model.addAttribute("titulo", "Nueva Pelicula");
		Pelicula pelicula = new Pelicula();
		List<Actor> listaActores = actoresService.buscarTodosSinPaginar();
		model.addAttribute("pelicula", pelicula);
		model.addAttribute("listaActores", listaActores);
		return "peliculas/formPelicula";
	}

	@GetMapping("/buscar")
	public String buscar(Model model) {
		return "peliculas/searchPelicula";
	}

	@GetMapping("/listado")
	public String listadoPeliculas(Model model, @RequestParam(name = "page", defaultValue = "0") int page) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarTodas(pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/peliculas/listado", listado);
		model.addAttribute("titulo", "Listado de todas las Peliculas");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "peliculas/listPelicula";
	}

	@GetMapping("/idPelicula/{id}")
	public String buscarPeliculaPorId(Model model, @PathVariable("id") Integer id) {
		Pelicula Pelicula = peliculasService.buscarPeliculaPorId(id);
		model.addAttribute("pelicula", Pelicula);
		return "peliculas/formPelicula";
	}

	@GetMapping("/titulo")
	public String buscarPorTitulo(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam("titulo") String titulo) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarPorTitulo(titulo, pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/peliculas/listado", listado);
		model.addAttribute("titulo", "Listado de Películas por título");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "peliculas/listPelicula";
	}

	@GetMapping("/genero")
	public String buscarPorGenero(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam("genero") String categoria) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarPorGenero(categoria, pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/peliculas/listado", listado);
		model.addAttribute("titulo", "Listado de Películas por género");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "peliculas/listPelicula";
	}

	@GetMapping("/actor")
	public String buscarPorActor(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam("actor") String actor) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Pelicula> listado = peliculasService.buscarPorActor(actor, pageable);
		PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/peliculas/listado", listado);
		model.addAttribute("titulo", "Listado de Películas por actor");
		model.addAttribute("listadoPeliculas", listado);
		model.addAttribute("page", pageRender);
		return "peliculas/listPelicula";
	}

	@PostMapping("/guardar/")
	public String guardarPelicula(Model model, Pelicula pelicula, RedirectAttributes attributes,
			@RequestParam("file") MultipartFile foto) {

		if (pelicula != null) {
			System.out.println(pelicula.getTitulo());
		}

		if (!foto.isEmpty()) {

			if (pelicula.getIdPelicula() != null && pelicula.getIdPelicula() > 0 && pelicula.getImagen() != null
					&& pelicula.getImagen().length() > 0) {

				uploadFileService.delete(pelicula.getImagen());
			}

			String uniqueFilename = null;
			try {
				uniqueFilename = uploadFileService.copy(foto);
			} catch (IOException e) {
				e.printStackTrace();
			}

			attributes.addFlashAttribute("msg", "Has subido correctamente '" + uniqueFilename + "'");

			pelicula.setImagen(uniqueFilename);
		}

		peliculasService.guardar(pelicula);
		model.addAttribute("titulo", "Nuevo Pelicula");
		attributes.addFlashAttribute("msg", "Los datos del Película fueron guardados!");

		return "redirect:/peliculas/listado";
	}

	@GetMapping("/editar/{id}")
	public String editar(Model model, @PathVariable("id") Integer id) {
		Pelicula pelicula = peliculasService.buscarPeliculaPorId(id);
		model.addAttribute("titulo", "Editar Pelicula");
		List<Actor> listaActores = actoresService.buscarTodosSinPaginar();

		model.addAttribute("listaActores", listaActores);
		model.addAttribute("pelicula", pelicula);

		return "peliculas/formPelicula";
	}

	@GetMapping("/borrar/{id}")
	public String eliminar(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
		peliculasService.eliminar(id);
		attributes.addFlashAttribute("msg", "Los datos del Película fueron borrados!");
		return "redirect:/peliculas/listado";
	}

	@GetMapping("/view/{id}")
	public String verDetalle(@PathVariable("id") int id, Model model) {
		Pelicula pelicula = peliculasService.buscarPeliculaPorId(id);
		Double notaMedia = criticasService.notaMedia(id);
		pelicula.setNotaMedia(notaMedia);
		//model.addAttribute("notaMedia", notaMedia);
		model.addAttribute("pelicula", pelicula);
		return "peliculas/detalle";
	}

	@GetMapping("/homePeliculas")
	public String homePeliculas() {
		return "peliculas/homePeliculas";
	}

	@GetMapping("/usuarios/signup")
	public String registrarse(Usuario usuario) {
		return "formRegistro";
	}

	@PostMapping("/usuarios/signup")
	public String guardarRegistro(Usuario usuario, RedirectAttributes attributes) {
		usuario.setEnable(true); // Activado por defecto
		Rol rol = new Rol();
		rol.setIdRol(2); // Perfil USUARIO
		usuario.setRoles(Arrays.asList(rol));
		usuarioService.guardar(usuario);
		attributes.addFlashAttribute("msg", "El registro fue guardado correctamente!");

		return "redirect:/login";
	}
}
