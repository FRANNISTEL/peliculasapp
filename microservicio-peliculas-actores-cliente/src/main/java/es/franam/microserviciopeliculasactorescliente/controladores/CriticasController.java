package es.franam.microserviciopeliculasactorescliente.controladores;

import java.security.Principal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.franam.microserviciopeliculasactorescliente.beans.Critica;
import es.franam.microserviciopeliculasactorescliente.beans.Usuario;
import es.franam.microserviciopeliculasactorescliente.paginador.PageRender;
import es.franam.microserviciopeliculasactorescliente.servicios.ICriticasService;
import es.franam.microserviciopeliculasactorescliente.servicios.IUsuariosService;

@Controller
@RequestMapping("/criticas")
public class CriticasController {

	@Autowired
	ICriticasService criticasService;

	@Autowired
	IUsuariosService usuariosService;

	@GetMapping(value = { "/", "/home", "" })
	public String home(Model model, @RequestParam(name = "page", defaultValue = "0") int page) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Critica> listado = criticasService.buscarTodas(pageable);
		PageRender<Critica> pageRender = new PageRender<Critica>("/criticas/listado", listado);
		model.addAttribute("listadoCriticas", listado);
		model.addAttribute("page", pageRender);
		return "home";

	}

	@GetMapping("/nuevo/{idPelicula}")
	public String nuevaCritica(Model model, @PathVariable("idPelicula") Integer idPelicula) {

		Critica critica = new Critica();
		critica.setIdPelicula(idPelicula);
		model.addAttribute("titulo", "Nueva Critica");
		model.addAttribute("critica", critica);
		return "criticas/formCriticas";
	}

	@GetMapping("/buscar")
	public String buscar(Model model) {
		return "criticas/searchCriticas";
	}

	@GetMapping("/listado")
	public String listadoCriticas(Model model, @RequestParam(name = "page", defaultValue = "0") int page) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Critica> listado = criticasService.buscarTodas(pageable);
		PageRender<Critica> pageRender = new PageRender<Critica>("/criticas/listado", listado);
		model.addAttribute("titulo", "Listado de todas las Criticas");
		model.addAttribute("listadoCriticas", listado);
		model.addAttribute("page", pageRender);
		return "criticas/listCriticas";
	}

	@GetMapping("/idCritica/{id}")
	public String buscarCriticaPorId(Model model, @PathVariable("id") Integer id) {
		Critica critica = criticasService.buscarCriticaPorId(id);
		model.addAttribute("Critica", critica);
		return "criticas/formCriticas";
	}

	@GetMapping("/{idPelicula}")
	public String buscarPorIdPelicula(Model model, @RequestParam(name = "page", defaultValue = "0") int page,
			@PathVariable("idPelicula") Integer idPelicula) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Critica> listado = criticasService.buscarCriticasPorIdPelicula(idPelicula, pageable);
		PageRender<Critica> pageRender = new PageRender<Critica>("/listado", listado);
		model.addAttribute("titulo", "Listado de Criticas por titulo");
		model.addAttribute("listadoCriticas", listado);
		model.addAttribute("page", pageRender);
		return "criticas/listCriticas";
	}

	@PostMapping("/guardar/")
	public String guardarCritica(Model model, Critica critica, RedirectAttributes attributes) {
		criticasService.guardar(critica);
		model.addAttribute("titulo", "Nueva Critica");
		attributes.addFlashAttribute("msg", "Los datos del Critica fueron guardados!");

		return "redirect:/criticas/listado";
	}

	@GetMapping("/editar/{id}")
	public String editar(Model model, @PathVariable("id") Integer id) {
		Critica critica = criticasService.buscarCriticaPorId(id);
		model.addAttribute("titulo", "Editar Crítica");
		model.addAttribute("critica", critica);

		return "criticas/formCriticas";
	}

	@GetMapping("/borrar/{id}")
	public String eliminar(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
		criticasService.eliminar(id);
		attributes.addFlashAttribute("msg", "Los datos de la Critica fueron borrados!");
		return "redirect:/criticas/listado";
	}

	@GetMapping("/view/{id}")
	public String verDetalle(@PathVariable("id") int id, Model model) {
		Critica critica = criticasService.buscarCriticaPorId(id);
		model.addAttribute("critica", critica);

		return "criticas/detalle";
	}

	@GetMapping("/homeCriticas")
	public String homeCriticas() {
		return "criticas/homeCriticas";
	}

	@PostMapping("/criticar/{idPelicula}")
	public String matricular(Critica critica, RedirectAttributes attributes, Principal principal) {
		Usuario usuario = usuariosService.buscarPorCorreo(principal.getName());
		Date date = new Date();
		critica.setFecha(date);
		critica.setUsuario(usuario);
		String resultado = criticasService.guardarCritica(critica);
		attributes.addFlashAttribute("msg", resultado);
		return "redirect:/peliculas/listado";
	}
}
