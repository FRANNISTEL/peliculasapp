package es.franam.microserviciopeliculasactorescliente.controladores;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.franam.microserviciopeliculasactorescliente.beans.Actor;
import es.franam.microserviciopeliculasactorescliente.paginador.PageRender;
import es.franam.microserviciopeliculasactorescliente.servicios.IActoresService;

@Controller
@RequestMapping("/actores")
public class ActorController {

	@Autowired
	IActoresService actoresService;

	@GetMapping("/nuevo")
	public String nuevo(Model model) {
		model.addAttribute("titulo", "Nuevo Actor");
		Actor actor = new Actor();
		model.addAttribute("actor", actor);
		return "actores/formActor";
	}

	@GetMapping("/buscar")
	public String buscar(Model model) {
		return "actores/searchActor";
	}

	@GetMapping("/listado")
	public String listadoActores(Model model, @RequestParam(name = "page", defaultValue = "0") int page) {
		Pageable pageable = PageRequest.of(page, 5);
		Page<Actor> listado = actoresService.buscarTodos(pageable);
		PageRender<Actor> pageRender = new PageRender<Actor>("/actores/listado", listado);
		model.addAttribute("titulo", "Listado de todos los Actores");
		model.addAttribute("listadoActores", listado);
		model.addAttribute("page", pageRender);
		return "actores/listActor";
	}

	@GetMapping("/idActor/{id}")
	public String buscarActorPorId(Model model, @PathVariable("id") Integer id) {
		Actor actor = actoresService.buscarActorPorId(id);
		model.addAttribute("actor", actor);
		return "actores/formActor";
	}

	@PostMapping("/guardar/")
	public String guardarActor(Model model, Actor actor, BindingResult result, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return "actores/formActor";
		}

		actoresService.guardar(actor);
		model.addAttribute("titulo", "Nuevo Actor");
		attributes.addFlashAttribute("msg", "Los datos del Actor fueron guardados!");
		return "redirect:/actores/listado";
	}

	@GetMapping("/editar/{id}")
	public String editar(Model model, @PathVariable("id") Integer id) {
		Actor actor = actoresService.buscarActorPorId(id);
		model.addAttribute("titulo", "Editar Actor");
		model.addAttribute("actor", actor);
		return "actores/formActor";
	}

	@GetMapping("/borrar/{id}")
	public String eliminar(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
		actoresService.eliminar(id);
		attributes.addFlashAttribute("msg", "Los datos del Actor fueron borrados!");
		return "redirect:/actores/listado";
	}

	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));

	}

	@GetMapping("/homeActores")
	public String homePeliculas() {
		return "actores/homeActores";
	}

}
