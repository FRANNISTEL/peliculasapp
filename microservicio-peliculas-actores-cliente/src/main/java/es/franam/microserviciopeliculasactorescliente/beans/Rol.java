package es.franam.microserviciopeliculasactorescliente.beans;

public class Rol {

	private Integer idRol;
	private String authority;

	public Rol(Integer idRol, String authority) {
		this.idRol = idRol;
		this.authority = authority;
	}

	public Rol() {
	}

	public Integer getIdRol() {
		return idRol;

	}

	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

}
