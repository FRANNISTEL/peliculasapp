package es.franam.microserviciopeliculasactorescliente.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Actor implements Serializable {

	private Integer idActor;

	private String nombre;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "es-ES", timezone = "Europe/Madrid")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;

	private String pais;

	private boolean isSelected = false;

	private List<Pelicula> peliculas;

	public Actor() {
	}

	public Actor(Integer idActor, String nombre, Date fecha, String pais, List<Pelicula> peliculas) {
		this.idActor = idActor;
		this.nombre = nombre;
		this.fecha = fecha;
		this.pais = pais;
		this.peliculas = peliculas;
	}

	public Integer getIdActor() {
		return idActor;
	}

	public void setIdActor(Integer idActor) {
		this.idActor = idActor;
	}

	public List<Pelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(List<Pelicula> peliculas) {
		this.peliculas = peliculas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idActor == null) ? 0 : idActor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Actor other = (Actor) obj;
		if (idActor == null) {
			if (other.idActor != null)
				return false;
		} else if (!idActor.equals(other.idActor))
			return false;
		return true;
	}

	private static final long serialVersionUID = 1L;

}
