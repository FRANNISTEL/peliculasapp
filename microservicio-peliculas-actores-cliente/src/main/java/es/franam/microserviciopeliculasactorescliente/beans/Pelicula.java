package es.franam.microserviciopeliculasactorescliente.beans;

import java.util.List;

public class Pelicula {

	private Integer idPelicula;

	private String titulo;

	private int anyo;

	private int duracion;

	private String pais;

	private String direccion;

	private String genero;

	private String sinopsis;

	private List<Actor> actores;

	private String imagen = "no-image.png";
	
	private Double notaMedia;

	public Pelicula(Integer idPelicula, String titulo, int anyo, int duracion, String pais, String direccion,
			String genero, String sinopsis, List<Actor> actores, String imagen) {

		this.idPelicula = idPelicula;
		this.titulo = titulo;
		this.anyo = anyo;
		this.duracion = duracion;
		this.pais = pais;
		this.direccion = direccion;
		this.genero = genero;
		this.sinopsis = sinopsis;
		this.actores = actores;
		this.imagen = imagen;
	}

	public Pelicula() {

	}
	
	public Double getNotaMedia() {
		return notaMedia;
	}

	public void setNotaMedia(Double notaMedia) {
		this.notaMedia = notaMedia;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public List<Actor> getActores() {
		return actores;
	}

	public void setActores(List<Actor> actores) {
		this.actores = actores;
	}

	public Integer getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAnyo() {
		return anyo;
	}

	public void setAnyo(int anyo) {
		this.anyo = anyo;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getSinopsis() {
		return sinopsis;
	}

	public void setSinopsis(String sinopsis) {
		this.sinopsis = sinopsis;
	}

	public void addAutor(Actor actor) {
		this.actores.add(actor);
	}
}
