package es.franam.microserviciopeliculasactorescliente.beans;

import java.util.Date;
import java.util.Objects;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Critica {

	private Integer idCritica;
	private Integer idPelicula;
	private String valoracion;
	private Integer nota;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "es-ES", timezone = "Europe/Madrid")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;
	private Usuario usuario;

	public Critica() {
	}

	public Critica(Integer idPelicula, Usuario usuario) {
		this.idPelicula = idPelicula;
		this.usuario = usuario;
	}

	public Integer getIdCritica() {
		return idCritica;
	}

	public void setIdCritica(Integer idCritica) {
		this.idCritica = idCritica;
	}

	public Integer getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}

	public String getValoracion() {
		return valoracion;
	}

	public void setValoracion(String valoracion) {
		this.valoracion = valoracion;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Critica))
			return false;
		Critica matricula = (Critica) o;
		return Objects.equals(idCritica, matricula.idCritica) && Objects.equals(idPelicula, matricula.idPelicula)
				&& Objects.equals(nota, matricula.nota) && Objects.equals(fecha, matricula.fecha)
				&& Objects.equals(usuario, matricula.usuario);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idCritica, idPelicula, nota, fecha, usuario);
	}

}
