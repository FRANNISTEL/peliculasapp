package es.franam.microserviciopeliculasactorescliente.beans;

import java.util.List;
import java.util.Objects;

public class Usuario {

	private Integer idUsuario;
	private String nombre;
	private String clave;
	private String correo;
	private boolean enable;

	private List<Critica> criticas;

	private List<Rol> roles;

	public Usuario(Integer idUsuario, String nombre, String clave, String correo, boolean enable,
			List<Critica> criticas, List<Rol> roles) {

		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.clave = clave;
		this.correo = correo;
		this.enable = enable;
		this.criticas = criticas;
		this.roles = roles;
	}

	public Usuario() {
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public List<Critica> getCriticas() {
		return criticas;
	}

	public void setCriticas(List<Critica> criticas) {
		this.criticas = criticas;
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

	public void addMatricula(Critica critica) {
		getCriticas().add(critica);
		critica.setUsuario(this);
	}

	public void removeMatricula(Critica critica) {
		if (critica != null) {
			getCriticas().remove(critica);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Usuario))
			return false;
		Usuario usuario = (Usuario) o;
		return enable == usuario.enable && Objects.equals(idUsuario, usuario.idUsuario)
				&& Objects.equals(nombre, usuario.nombre) && Objects.equals(clave, usuario.clave)
				&& Objects.equals(correo, usuario.correo) && Objects.equals(criticas, usuario.criticas)
				&& Objects.equals(roles, usuario.roles);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idUsuario, nombre, clave, correo, enable, criticas, roles);
	}

}
