package es.franam.microserviciopeliculasactorescliente.converters;

import java.util.HashSet;
import java.util.Set;

import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.stereotype.Component;

import es.franam.microserviciopeliculasactorescliente.beans.Actor;

@Component
public class ActorConverter implements GenericConverter {
	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		Set<ConvertiblePair> set = new HashSet<>();

		set.add(new ConvertiblePair(String.class, Actor.class));
		set.add(new ConvertiblePair(Actor.class, String.class));
		return set;

	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		if (sourceType.getType() == String.class && targetType.getType() == Actor.class
				&& !((String) source).isEmpty()) {
			Actor a = new Actor();
			a.setIdActor(Integer.parseInt((String) source));
			return a;
		}
		if (sourceType.getType() == Actor.class && targetType.getType() == String.class) {
			Actor a = (Actor) source;

			return a.getIdActor().toString();
		}
		return null;
	}
}