package es.franam.microserviciopeliculasactorescliente.servicios;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import es.franam.microserviciopeliculasactorescliente.beans.Critica;
import es.franam.microserviciopeliculasactorescliente.beans.Usuario;

@Service
public class CriticasServiceImpl implements ICriticasService {

	@Autowired
	RestTemplate template;
	String url = "http://localhost:8090/api/zusuarios/criticas/criticas";

	@Autowired
	IUsuariosService usuariosService;

	@Autowired
	IPeliculasService peliculaService;

	@Override
	public Page<Critica> buscarTodas(Pageable pageable) {
		Critica[] criticas = template.getForObject(url, Critica[].class);
		List<Critica> criticasList = Arrays.asList(criticas);
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<Critica> list;
		if (criticasList.size() < startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, criticasList.size());
			list = criticasList.subList(startItem, toIndex);
		}
		Page<Critica> page = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), criticasList.size());
		return page;
	}

	@Override
	public Critica buscarCriticaPorId(Integer idCritica) {
		Critica critica = template.getForObject(url + "/" + idCritica, Critica.class);
		return critica;

	}

	@Override
	public void guardar(Critica critica) {
		if (critica.getIdCritica() != null && critica.getIdCritica() > 0) {
			template.put(url, critica);
		} else {
			critica.setIdCritica(0);
			template.postForObject(url, critica, String.class);
		}
	}

	@Override
	public void eliminar(Integer idCritica) {
		template.delete(url + "/" + idCritica);
	}

	@Override
	public Page<Critica> buscarCriticasPorIdPelicula(Integer idPelicula, Pageable pageable) {

		Critica[] criticas = template.getForObject(url + "/idPelicula/" + idPelicula, Critica[].class);
		List<Critica> criticasList = Arrays.asList(criticas);
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<Critica> list;
		if (criticasList.size() < startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, criticasList.size());
			list = criticasList.subList(startItem, toIndex);
		}
		Page<Critica> page = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), criticasList.size());
		return page;
	}

	@Override
	public String guardarCritica(Critica critica) {
		if (critica.getIdCritica() != null && critica.getIdCritica() > 0) {
			return "No se puede modificar una critica.";
		} else {

			Usuario usuario = usuariosService.buscarUsuarioPorId(critica.getUsuario().getIdUsuario());
			String resultado = "";

			List<Critica> criticas = usuario.getCriticas();
			boolean b = false;
			for (Critica c : criticas) {
				if (critica.getIdPelicula() == c.getIdPelicula()) {
					b = true;
				}
			}
			if (b) {
				return "El usuario ya ha realizado una critica a esta pelicula!";
			}

			usuariosService.realizarCritica(usuario.getIdUsuario(), critica.getIdPelicula());
			critica.setIdCritica(0);
			critica.setFecha(new Date());
			template.postForObject(url, critica, String.class);
			return resultado + "Los datos de la critica fueron guardados!";
		}
	}

	@Override
	public Double notaMedia(int id) {
		Critica[] criticas = template.getForObject(url + "/idPelicula/" + id, Critica[].class);
		List<Critica> listado= Arrays.asList(criticas);
		Double notaMedia= listado.stream().collect(Collectors.averagingDouble(Critica::getNota));
		return notaMedia;
	}

}
