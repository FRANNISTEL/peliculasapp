package es.franam.microserviciopeliculasactorescliente.servicios;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import es.franam.microserviciopeliculasactorescliente.beans.Usuario;

public interface IUsuariosService {

	Page<Usuario> buscarTodos(Pageable pageable);

	Usuario buscarUsuarioPorId(Integer idUsuario);

	void guardar(Usuario usuario);

	void eliminar(Integer idUsuario);

	Page<Usuario> buscarPorNombre(String nombre, Pageable pageable);

	Usuario buscarPorCorreo(String correo);

	Usuario login(String correo, String clave);

	void realizarCritica(Integer idUsuario, Integer idPelicula);
}
