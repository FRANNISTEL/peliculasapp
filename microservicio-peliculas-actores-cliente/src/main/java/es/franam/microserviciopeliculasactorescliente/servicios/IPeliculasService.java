package es.franam.microserviciopeliculasactorescliente.servicios;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import es.franam.microserviciopeliculasactorescliente.beans.Pelicula;

public interface IPeliculasService {

	Page<Pelicula> buscarTodas(Pageable pageable);

	Pelicula buscarPeliculaPorId(Integer idPelicula);

	Page<Pelicula> buscarPorTitulo(String titulo, Pageable pageable);

	Page<Pelicula> buscarPorGenero(String genero, Pageable pageable);

	Page<Pelicula> buscarPorActor(String actor, Pageable pageable);

	void guardar(Pelicula pelicula);

	void eliminar(Integer idPelicula);

}
