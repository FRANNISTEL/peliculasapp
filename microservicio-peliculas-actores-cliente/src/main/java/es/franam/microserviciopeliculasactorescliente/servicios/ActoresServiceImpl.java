package es.franam.microserviciopeliculasactorescliente.servicios;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import es.franam.microserviciopeliculasactorescliente.beans.Actor;

@Service
public class ActoresServiceImpl implements IActoresService {

	@Autowired
	RestTemplate template;
	String url = "http://localhost:8090/api/zpeliculas/actores";

	@Override
	public Page<Actor> buscarTodos(Pageable pageable) {
		Actor[] actores = template.getForObject(url + "/actores", Actor[].class);
		List<Actor> actoresList = Arrays.asList(actores);
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		List<Actor> list;
		if (actoresList.size() < startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, actoresList.size());
			list = actoresList.subList(startItem, toIndex);
		}
		Page<Actor> page = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), actoresList.size());
		return page;
	}

	@Override
	public Actor buscarActorPorId(Integer idActor) {
		Actor actor = template.getForObject(url + "/actores/" + idActor, Actor.class);
		return actor;
	}

	@Override
	public void guardar(Actor actor) {
		if (actor.getIdActor() != null && actor.getIdActor() > 0) {
			template.put(url + "/actores", actor);
		} else {
			actor.setIdActor(0);
			template.postForObject(url + "/actores", actor, String.class);
		}
	}

	@Override
	public void eliminar(Integer idActor) {
		template.delete(url + "/actores/" + idActor);
	}

	@Override
	public List<Actor> buscarTodosSinPaginar() {
		Actor[] actores = template.getForObject(url + "/actores", Actor[].class);
		return Arrays.asList(actores);
	}

}
