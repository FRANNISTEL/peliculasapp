package es.franam.microserviciopeliculasactorescliente.servicios;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import es.franam.microserviciopeliculasactorescliente.beans.Critica;

public interface ICriticasService {

	Page<Critica> buscarTodas(Pageable pageable);

	Critica buscarCriticaPorId(Integer idCritica);

	Page<Critica> buscarCriticasPorIdPelicula(Integer idPelicula, Pageable pageable);

	void guardar(Critica critica);

	void eliminar(Integer idCritica);

	String guardarCritica(Critica critica);

	Double notaMedia(int id);
}
