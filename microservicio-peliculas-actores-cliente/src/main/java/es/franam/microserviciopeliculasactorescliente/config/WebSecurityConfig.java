package es.franam.microserviciopeliculasactorescliente.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomAuthenticationProvider authProvider;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable().authorizeRequests()
				.antMatchers("/peliculas/view/**", "/bootstrap/**", "/js/**", "/css/**", "/login", "/usuarios/signup")
				.permitAll()

				// Asignar permisos a URLs por ROLES
				.antMatchers("/usuarios/**").hasAnyAuthority("ROLE_ADMIN")
				// .antMatchers("/peliculas/**").hasAnyAuthority("ROLE_ADMIN")
				.antMatchers("/actores/**").hasAnyAuthority("ROLE_ADMIN").antMatchers("/criticas/**")
				.hasAnyAuthority("ROLE_ADMIN", "ROLE_USER")

				.anyRequest().authenticated().and().formLogin().loginPage("/login").defaultSuccessUrl("/", true);
	}

}
