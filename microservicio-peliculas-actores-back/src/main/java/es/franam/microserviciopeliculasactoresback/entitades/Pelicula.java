package es.franam.microserviciopeliculasactoresback.entitades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "peliculas", schema = "db_film")
public class Pelicula implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPelicula;
	@NotEmpty
	@Size(min = 2, max = 25)
	@Column(nullable = false)
	private String titulo;
	// @Min(1900)
	@Column(nullable = false)
	private int anyo;
	@Range(min = 50, max = 240, message = "debe estar entre 50 y 240")
	@Column(nullable = false)
	private int duracion;
	@NotEmpty
	@Column(nullable = false)
	private String pais;
	@NotEmpty
	@Column(nullable = false)
	private String direccion;
	@NotEmpty
	@Column(nullable = false)
	private String genero;
	@NotEmpty
	@Column(nullable = false)
	private String sinopsis;
	private String imagen;

	@JoinTable(name = "peliculas_actores", joinColumns = @JoinColumn(name = "id_actor"), inverseJoinColumns = @JoinColumn(name = "id_pelicula"))
	@ManyToMany(fetch = FetchType.EAGER)
	@JsonIgnoreProperties("peliculas")
	private List<Actor> actores;

	public List<Actor> getActores() {
		return actores;
	}

	public void setActores(List<Actor> actores) {
		this.actores = actores;
	}

	public Long getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(Long idPelicula) {
		this.idPelicula = idPelicula;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAnyo() {
		return anyo;
	}

	public void setAnyo(int anyo) {
		this.anyo = anyo;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getSinopsis() {
		return sinopsis;
	}

	public void setSinopsis(String sinopsis) {
		this.sinopsis = sinopsis;
	}

	public void addActor(Actor actor) {
		if (actor != null) {
			getActores().add(actor);
		}
	}

	public void removeActor(Actor actor) {
		if (actor != null) {
			getActores().remove(actor);
		}
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	private static final long serialVersionUID = 1L;

}
