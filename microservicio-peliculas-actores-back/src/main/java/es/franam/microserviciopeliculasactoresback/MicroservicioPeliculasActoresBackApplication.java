package es.franam.microserviciopeliculasactoresback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroservicioPeliculasActoresBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicioPeliculasActoresBackApplication.class, args);

	}

}
