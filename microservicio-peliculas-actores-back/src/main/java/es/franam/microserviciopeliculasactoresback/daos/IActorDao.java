package es.franam.microserviciopeliculasactoresback.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import es.franam.microserviciopeliculasactoresback.entitades.Actor;

public interface IActorDao extends JpaRepository<Actor, Long> {

}
