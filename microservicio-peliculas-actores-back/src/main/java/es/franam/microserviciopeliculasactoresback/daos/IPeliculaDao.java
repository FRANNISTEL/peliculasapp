package es.franam.microserviciopeliculasactoresback.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import es.franam.microserviciopeliculasactoresback.entitades.Pelicula;

public interface IPeliculaDao extends JpaRepository<Pelicula, Long> {

	List<Pelicula> findByTituloContainingIgnoreCase(String titulo);

	List<Pelicula> findByGeneroContainingIgnoreCase(String genero);

	List<Pelicula> findByActoresNombreContainingIgnoreCase(String nombre);

}
