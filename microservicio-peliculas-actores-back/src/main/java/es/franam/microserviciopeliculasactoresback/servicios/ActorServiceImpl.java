package es.franam.microserviciopeliculasactoresback.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.franam.microserviciopeliculasactoresback.daos.IActorDao;
import es.franam.microserviciopeliculasactoresback.entitades.Actor;

@Service
public class ActorServiceImpl implements IActorService {

	@Autowired
	private IActorDao actorDao;

	@Override
	@Transactional(readOnly = true)
	public List<Actor> buscarTodos() {
		return actorDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Actor buscarPorId(Long id) {
		return actorDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Actor guardar(Actor actor) {
		return actorDao.save(actor);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		Optional<Actor> optionalActor = actorDao.findById(id);
		if (optionalActor.isPresent()) {
			Actor actor = optionalActor.get();
			actor.getPeliculas().forEach(pelicula -> pelicula.removeActor(actor));
		}
		actorDao.deleteById(id);

	}

}
