package es.franam.microserviciopeliculasactoresback.servicios;

import java.util.List;

import es.franam.microserviciopeliculasactoresback.entitades.Actor;

public interface IActorService {

	public List<Actor> buscarTodos();

	public Actor buscarPorId(Long id);

	public Actor guardar(Actor actor);

	public void eliminar(Long id);

}
