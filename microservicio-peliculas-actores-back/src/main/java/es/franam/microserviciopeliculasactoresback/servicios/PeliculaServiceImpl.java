package es.franam.microserviciopeliculasactoresback.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.franam.microserviciopeliculasactoresback.daos.IPeliculaDao;
import es.franam.microserviciopeliculasactoresback.entitades.Pelicula;

@Service
public class PeliculaServiceImpl implements IPeliculaService {

	@Autowired
	private IPeliculaDao peliculaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Pelicula> buscarTodas() {
		return peliculaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Pelicula buscarPorId(Long id) {
		return peliculaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Pelicula guardar(Pelicula pelicula) {

		return peliculaDao.save(pelicula);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		peliculaDao.deleteById(id);
	}

	@Override
	@Transactional
	public Pelicula actualizar(Pelicula pelicula) {
		return guardar(pelicula);
	}

	@Override
	public List<Pelicula> buscarPorTitulo(String titulo) {
		return peliculaDao.findByTituloContainingIgnoreCase(titulo);
	}

	@Override
	public List<Pelicula> buscarPorGenero(String genero) {
		return peliculaDao.findByGeneroContainingIgnoreCase(genero);
	}

	@Override
	public List<Pelicula> buscarPorActor(String nombre) {
		return peliculaDao.findByActoresNombreContainingIgnoreCase(nombre);
	}

}
