package es.franam.microserviciopeliculasactoresback.servicios;

import java.util.List;

import es.franam.microserviciopeliculasactoresback.entitades.Pelicula;

public interface IPeliculaService {

	public List<Pelicula> buscarTodas();

	public Pelicula buscarPorId(Long id);

	public Pelicula guardar(Pelicula pelicula);

	public void eliminar(Long id);

	public Pelicula actualizar(Pelicula pelicula);

	public List<Pelicula> buscarPorTitulo(String titulo);

	public List<Pelicula> buscarPorGenero(String genero);

	public List<Pelicula> buscarPorActor(String nombre);

}
