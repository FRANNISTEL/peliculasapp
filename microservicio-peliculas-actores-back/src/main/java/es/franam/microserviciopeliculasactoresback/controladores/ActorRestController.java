package es.franam.microserviciopeliculasactoresback.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.franam.microserviciopeliculasactoresback.entitades.Actor;
import es.franam.microserviciopeliculasactoresback.servicios.IActorService;

@RestController
@RequestMapping("/actores")
public class ActorRestController {

	@Autowired
	private IActorService actorService;

	@GetMapping("/actores")
	public List<Actor> listar() {
		return actorService.buscarTodos();
	}

	@GetMapping("/actores/{id}")
	public Actor buscarPorId(@PathVariable Long id) {
		return actorService.buscarPorId(id);
	}

	@PostMapping("/actores")
	public Actor guardar(@RequestBody Actor actor) {
		return actorService.guardar(actor);
	}

	@PutMapping("/actores")
	public Actor actualizar(@RequestBody Actor actor) {
		return actorService.guardar(actor);
	}

	@DeleteMapping("/actores/{id}")
	public void eliminar(@PathVariable Long id) {
		actorService.eliminar(id);
	}

}
