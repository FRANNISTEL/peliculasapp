package es.franam.microserviciopeliculasactoresback.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.franam.microserviciopeliculasactoresback.entitades.Pelicula;
import es.franam.microserviciopeliculasactoresback.servicios.IPeliculaService;

@RestController
@RequestMapping("/peliculas")
public class PeliculaRestController {

	@Autowired
	private IPeliculaService peliculaService;

	@GetMapping("/peliculas")
	public List<Pelicula> listar() {
		return peliculaService.buscarTodas();
	}

	@GetMapping("/peliculas/{id}")
	public Pelicula buscarPorId(@PathVariable Long id) {
		return peliculaService.buscarPorId(id);
	}

	@PostMapping("/peliculas")
	public Pelicula guardar(@RequestBody Pelicula pelicula) {

		return peliculaService.guardar(pelicula);
	}

	@PutMapping("/peliculas")
	public Pelicula actualizar(@RequestBody Pelicula pelicula) {
		return peliculaService.actualizar(pelicula);

	}

	@DeleteMapping("/peliculas/{id}")
	public void eliminar(@PathVariable Long id) {
		peliculaService.eliminar(id);
	}

	@GetMapping("/peliculas/titulo/{titulo}")
	public List<Pelicula> buscarPorTitulo(@PathVariable String titulo) {
		return peliculaService.buscarPorTitulo(titulo);
	}

	@GetMapping("/peliculas/genero/{genero}")
	public List<Pelicula> buscarPorGenero(@PathVariable String genero) {
		return peliculaService.buscarPorGenero(genero);
	}

	@GetMapping("/peliculas/actor/{nombre}")
	public List<Pelicula> buscarPorActor(@PathVariable String nombre) {
		return peliculaService.buscarPorActor(nombre);
	}

}
