INSERT INTO peliculas(titulo,anyo,duracion,pais,direccion,genero,sinopsis,imagen) VALUES("Los odiosos ocho",2015,167,"Estados Unidos","Quentin Tarantino","OESTE","La película no podía empezar mejor, con una presentación demorada que se recrea en el paisaje nevado, todo ello acompañado por la partitura de Morricone", LOAD_FILE('C:\imagenes\odiosos.jpg'));
INSERT INTO peliculas(titulo,anyo,duracion,pais,direccion,genero,sinopsis,imagen) VALUES("El Padrino",1972,200,"Estados Unidos","Francis Ford Coppola","DRAMA"," La historia de la familia mafiosa comandada por Vito Corleone se ha convertido en uno de los mayores clásicos de la historia del cine", LOAD_FILE('C:/imagenes/padrino.jpg'));
INSERT INTO peliculas(titulo,anyo,duracion,pais,direccion,genero,sinopsis,imagen) VALUES("El mago de Oz",1939,167,"Estados Unidos","Victor Fleming","COMEDIA","una niña estadounidense es arrastrada por un tornado en el estado de Kansas hasta una tierra de fantasía ", LOAD_FILE('C:/imagenes/mago.jpg')); 
INSERT INTO peliculas(titulo,anyo,duracion,pais,direccion,genero,sinopsis,imagen) VALUES("Ciudadano Kane",1941,190,"Estados Unidos","Orson Welles","DRAMA","Un importante financiero estadounidense, Charles Foster Kane, dueño de una importante cadena de periódicos, de una red de emisoras", LOAD_FILE('C:/imagenes/ciudadano.jpeg'));
INSERT INTO peliculas(titulo,anyo,duracion,pais,direccion,genero,sinopsis,imagen) VALUES("Sueño de libertad ",1994,180,"Estados Unidos","Frank Darabont","DRAMA","Basada en la novela corta de Stephen King, Rita Hayworth y la redención de Shawshank, el film abarca una mirada optimista de la vida", LOAD_FILE('C:/imagenes/sueno.jpg'));
INSERT INTO peliculas(titulo,anyo,duracion,pais,direccion,genero,sinopsis,imagen) VALUES("Pulp Fiction",1994,140,"Estados Unidos","Quentin Tarantino","POLICIACA","Jules y Vincent, dos asesinos a sueldo con no demasiadas luces, trabajan para el gángster Marsellus Wallace", LOAD_FILE('C:/imagenes/pulp.jpg'));


INSERT INTO actores(nombre,fecha,pais) VALUES("Kurt Russell",'1952/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("John Travolta",'1950/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Samuel L. Jackson",'1952/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Marlon Brando",'1940/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Francis Ford Coppola",'1945/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Al Pacino",'1942/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Judy Garland",'1928/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Frank Morgan",'1932/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Orson Welles",'1925/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Joseph Cotten",'1923/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Morgan Freeman",'1935/03/15 02:53:50.000000000',"Estados Unidos");
INSERT INTO actores(nombre,fecha,pais) VALUES("Tim Robbins",'1960/03/15 02:53:50.000000000',"Estados Unidos");


INSERT INTO peliculas_actores VALUES (1, 1);
INSERT INTO peliculas_actores VALUES (1, 3);
INSERT INTO peliculas_actores VALUES (2, 4);
INSERT INTO peliculas_actores VALUES (2, 5);
INSERT INTO peliculas_actores VALUES (2, 6);
INSERT INTO peliculas_actores VALUES (3, 7);
INSERT INTO peliculas_actores VALUES (3, 10);
INSERT INTO peliculas_actores VALUES (3, 8);
INSERT INTO peliculas_actores VALUES (4, 9);
INSERT INTO peliculas_actores VALUES (5, 11);
INSERT INTO peliculas_actores VALUES (5, 12);
INSERT INTO peliculas_actores VALUES (6, 2);
INSERT INTO peliculas_actores VALUES (6, 3);


