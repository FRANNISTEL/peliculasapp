package es.franam.microserviciousuarioscriticas.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.franam.microserviciousuarioscriticas.entidades.Usuario;
import es.franam.microserviciousuarioscriticas.servicios.IUsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	IUsuarioService usuarioService;

	@GetMapping("/usuarios")
	public List<Usuario> buscarTodos() {
		return usuarioService.buscarTodos();
	}

	@GetMapping("/usuarios/{id}")
	public Usuario buscarUsuarioPorId(@PathVariable Integer id) {
		return usuarioService.buscarUsuarioPorId(id);
	}

	@GetMapping("/usuarios/correo/{correo}")
	public Usuario buscarUsuarioPorCorreo(@PathVariable String correo) {
		return usuarioService.buscarUsuarioPorCorreo(correo);
	}

	@GetMapping("/usuarios/nombre/{nombre}")
	public List<Usuario> buscarUsuarioPorNombre(@PathVariable String nombre) {
		return usuarioService.buscarUsuarioPorNombre(nombre);
	}

	@PostMapping("/usuarios")
	public void guardarUsuario(@RequestBody Usuario usuario) {
		usuarioService.guardarUsuario(usuario);
	}

	@PutMapping("/usuarios")
	public void actualizarUsuario(@RequestBody Usuario usuario) {
		usuarioService.actualizarUsuario(usuario);
	}

	@DeleteMapping("/usuarios/{id}")
	public void eliminarUsuario(@PathVariable Integer id) {
		usuarioService.eliminarUsuario(id);
	}

	@GetMapping("/usuarios/login/{correo}/{clave}")
	public Usuario buscarUsuarioPorCorreoConClave(@PathVariable("correo") String correo,
			@PathVariable("clave") String clave) {
		return usuarioService.buscarUsuarioPorCorreoClave(correo, clave);
	}

	@GetMapping("/usuarios/insc/{idu}/{idp}")
	public void inscribirCurso(@PathVariable("idu") Integer idu, @PathVariable("idp") Integer idp) {
		usuarioService.realizarCritica(idu, idp);
	}
}
