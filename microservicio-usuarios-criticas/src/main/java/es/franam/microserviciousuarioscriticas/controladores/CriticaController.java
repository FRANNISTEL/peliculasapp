package es.franam.microserviciousuarioscriticas.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.franam.microserviciousuarioscriticas.entidades.Critica;
import es.franam.microserviciousuarioscriticas.servicios.ICriticaService;

@RestController
@RequestMapping("/criticas")
public class CriticaController {

	@Autowired
	ICriticaService criticaService;

	@GetMapping("/criticas")
	public List<Critica> buscarTodas() {
		return criticaService.buscarTodas();
	}

	@GetMapping("/criticas/idPelicula/{idPelicula}")
	public List<Critica> buscarCriticaPorIdPelicula(@PathVariable Integer idPelicula) {
		return criticaService.buscarCriticasPorIdPelicula(idPelicula);
	}

	@GetMapping("/criticas/{id}")
	public Critica buscarcriticaPorId(@PathVariable Integer id) {
		return criticaService.buscarCriticaPorId(id);
	}

	@PostMapping("/criticas")
	public void guardarCritica(@RequestBody Critica critica) {
		criticaService.guardarCritica(critica);
	}

	@PutMapping("/criticas")
	public void actualizarCritica(@RequestBody Critica critica) {
		criticaService.guardarCritica(critica);
	}

	@DeleteMapping("/criticas/{id}")
	public void eliminarcritica(@PathVariable Integer id) {
		criticaService.eliminarCritica(id);
	}
}
