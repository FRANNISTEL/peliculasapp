package es.franam.microserviciousuarioscriticas.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.franam.microserviciousuarioscriticas.daos.ICriticaDao;
import es.franam.microserviciousuarioscriticas.daos.IUsuarioDao;
import es.franam.microserviciousuarioscriticas.entidades.Critica;
import es.franam.microserviciousuarioscriticas.entidades.Usuario;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioDao usuarioDao;

	@Autowired
	private ICriticaDao criticaDao;

	@Override
	public List<Usuario> buscarTodos() {
		return usuarioDao.findAll();
	}

	@Override
	public Usuario buscarUsuarioPorId(Integer idUsuario) {
		return usuarioDao.findById(idUsuario).orElse(null);
	}

	@Override
	public List<Usuario> buscarUsuarioPorNombre(String nombre) {
		return usuarioDao.findByNombreContainingIgnoreCaseOrderByNombreAsc(nombre);
	}

	@Override
	public Usuario buscarUsuarioPorCorreo(String correo) {
		return usuarioDao.findByCorreo(correo);
	}

	@Override
	public void guardarUsuario(Usuario usuario) {
		usuarioDao.save(usuario);

	}

	@Override
	public void eliminarUsuario(Integer idUsuario) {
		usuarioDao.deleteById(idUsuario);

	}

	@Override
	public void actualizarUsuario(Usuario usuario) {
		usuarioDao.save(usuario);
	}

	@Override
	public void eliminarCritica(Integer idUsuario, Integer idCritica) {
		Usuario usuario = usuarioDao.findById(idUsuario).orElse(null);
		usuario.removeCritica(criticaDao.findById(idCritica).get());
	}

	@Override
	public Usuario buscarUsuarioPorCorreoClave(String correo, String clave) {
		return usuarioDao.findByCorreoAndClave(correo, clave).orElse(null);
	}

	@Override
	public void realizarCritica(Integer idUsuario, Integer IdPelicula) {
		Optional<Usuario> optionalUsuario = usuarioDao.findById(idUsuario);
		if (optionalUsuario.isPresent()) {
			Usuario usuario = optionalUsuario.get();
			Optional<Critica> optionalCritica = criticaDao.findById(IdPelicula);
			if (optionalCritica.isPresent()) {
				usuario.addCritica(optionalCritica.get());
				usuarioDao.save(usuario);
			}
		}
	}

}
