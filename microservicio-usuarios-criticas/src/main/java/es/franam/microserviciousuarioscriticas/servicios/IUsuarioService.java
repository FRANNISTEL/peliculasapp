package es.franam.microserviciousuarioscriticas.servicios;

import java.util.List;

import es.franam.microserviciousuarioscriticas.entidades.Usuario;

public interface IUsuarioService {

	List<Usuario> buscarTodos();

	Usuario buscarUsuarioPorId(Integer idUsuario);

	List<Usuario> buscarUsuarioPorNombre(String nombre);

	Usuario buscarUsuarioPorCorreo(String correo);

	void guardarUsuario(Usuario usuario);

	void eliminarUsuario(Integer idUsuario);

	void actualizarUsuario(Usuario usuario);

	void eliminarCritica(Integer idUsuario, Integer idCritica);

	Usuario buscarUsuarioPorCorreoClave(String correo, String clave);

	void realizarCritica(Integer idu, Integer idp);

}
