package es.franam.microserviciousuarioscriticas.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.franam.microserviciousuarioscriticas.daos.ICriticaDao;
import es.franam.microserviciousuarioscriticas.entidades.Critica;

@Service
public class CriticasServiceImpl implements ICriticaService {

	@Autowired
	ICriticaDao criticaDao;

	@Override
	public List<Critica> buscarTodas() {
		return criticaDao.findAll();
	}

	@Override
	public List<Critica> buscarCriticasPorIdPelicula(Integer idPelicula) {
		return criticaDao.findByIdPelicula(idPelicula);
	}

	@Override
	public Critica buscarCriticaPorId(Integer idCritica) {
		return criticaDao.findById(idCritica).orElse(null);
	}

	@Override
	public void guardarCritica(Critica critica) {
		criticaDao.save(critica);
	}

	@Override
	public void eliminarCritica(Integer idCritica) {
		criticaDao.deleteById(idCritica);

	}

}
