package es.franam.microserviciousuarioscriticas.servicios;

import java.util.List;

import es.franam.microserviciousuarioscriticas.entidades.Critica;

public interface ICriticaService {

	List<Critica> buscarTodas();

	List<Critica> buscarCriticasPorIdPelicula(Integer idPelicula);

	Critica buscarCriticaPorId(Integer idCritica);

	void guardarCritica(Critica critica);

	void eliminarCritica(Integer idCritica);
}
