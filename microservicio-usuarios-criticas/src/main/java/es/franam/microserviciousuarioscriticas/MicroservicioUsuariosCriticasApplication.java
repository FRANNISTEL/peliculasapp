package es.franam.microserviciousuarioscriticas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroservicioUsuariosCriticasApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicioUsuariosCriticasApplication.class, args);
	}

}
