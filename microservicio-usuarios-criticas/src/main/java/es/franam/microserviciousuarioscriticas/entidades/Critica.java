package es.franam.microserviciousuarioscriticas.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "criticas", schema = "usuarios_criticas")
public class Critica implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCritica;
	private Integer idPelicula;
	private String valoracion;
	@Max(10)
	@Min(0)
	private Integer nota;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "es-ES", timezone = "Europe/Madrid")
	@Temporal(TemporalType.DATE)
	private Date fecha;
	@ManyToOne
	@JoinColumn(name = "Users_idUsuario", referencedColumnName = "idUsuario", nullable = false)
	@JsonIgnoreProperties("criticas")
	private Usuario usuario;

	public Critica() {
	}

	public Integer getIdCritica() {
		return idCritica;
	}

	public void setIdCritica(Integer idCritica) {
		this.idCritica = idCritica;
	}

	public Integer getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}

	public String getValoracion() {
		return valoracion;
	}

	public void setValoracion(String valoracion) {
		this.valoracion = valoracion;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Critica))
			return false;
		Critica matricula = (Critica) o;
		return Objects.equals(idCritica, matricula.idCritica) && Objects.equals(idPelicula, matricula.idPelicula)
				&& Objects.equals(nota, matricula.nota) && Objects.equals(fecha, matricula.fecha)
				&& Objects.equals(usuario, matricula.usuario);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idCritica, idPelicula, nota, fecha, usuario);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
