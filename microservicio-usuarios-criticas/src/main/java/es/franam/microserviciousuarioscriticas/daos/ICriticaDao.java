package es.franam.microserviciousuarioscriticas.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import es.franam.microserviciousuarioscriticas.entidades.Critica;

public interface ICriticaDao extends JpaRepository<Critica, Integer> {

	List<Critica> findByIdPelicula(int idPelicula);

}
