package es.franam.microserviciousuarioscriticas.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import es.franam.microserviciousuarioscriticas.entidades.Usuario;

public interface IUsuarioDao extends JpaRepository<Usuario, Integer> {

	public List<Usuario> findByNombreContainingIgnoreCaseOrderByNombreAsc(String nombre);

	public Usuario findByCorreo(String correo);

	Optional<Usuario> findByCorreoAndClave(String correo, String clave);

}
