
INSERT INTO users(username,password,correo, enable) VALUES ('admin', 'admin', 'admin@uah.es', 1);
INSERT INTO users(username,password,correo, enable) VALUES ('user1', 'user1', 'user1@uah.es', 1);


INSERT INTO `authorities` VALUES (1, 'ROLE_ADMIN');
INSERT INTO `authorities` VALUES (2, 'ROLE_USER');

INSERT INTO `users_has_authorities` VALUES (1, 1);
INSERT INTO `users_has_authorities` VALUES (2, 2);


INSERT INTO criticas(idPelicula, valoracion, nota, fecha, Users_idUsuario) VALUES (1, "Buena", 10, '2011/03/15 02:53:50.000000000', 2);
INSERT INTO criticas(idPelicula, valoracion, nota, fecha, Users_idUsuario) VALUES (2, "Mala", 10, '2011/03/15 02:53:50.000000000', 2);
INSERT INTO criticas(idPelicula, valoracion, nota, fecha, Users_idUsuario) VALUES (3, "Genial", 25, '2011/03/15 02:53:50.000000000', 2);